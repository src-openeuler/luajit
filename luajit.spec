%global apiver %(v=2.1.0; echo ${v%.${v#[0-9].[0-9].}})

Name:           luajit
Version:        2.1.0
Release:        10
Summary:        Just-In-Time Compiler for Lua
License:        MIT
URL:            http://luajit.org/
Source0:        http://luajit.org/download/LuaJIT-2.1.0-beta3.tar.gz
Source1:        loongarch64.tar.gz
Source2:        loongarch64.conf
Source3:        apply-patches

# Patches from https://github.com/LuaJit/LuaJIT.git
# Generated from v2.1 branch against the 2.1.0-beta3 tag using
# git diff v2.1.0-beta3..v2.1 > luajit-2.1-update.patch
Patch0001:      luajit-2.1-d06beb0-update.patch
Patch0002:      0002-luajit-add-secure-compile-option-fstack.patch
Patch0003:      add-riscv-support.patch
ExclusiveArch:  %{arm} %{ix86} x86_64 %{mips} aarch64 riscv64 loongarch64

BuildRequires:  gcc
BuildRequires:  make

%description
LuaJIT is a Just-In-Time Compiler (JIT) for the Lua programming language. Lua is a powerful, dynamic and
light-weight programming language. It may be embedded or used as a general-purpose, stand-alone language.

%package        devel
Summary:        Development files for luajit
Requires:       luajit = 2.1.0-%{release}

%description    devel
This package contains development files for luajit.

%package        help
Summary: Documents for luajit

%description    help
Man pages and other related documents for luajit.

%prep
%setup -q -n LuaJIT-2.1.0-beta3
%patch -P0001 -p1
%patch -P0002 -p1

# riscv64 arch patch
%ifarch riscv64
%patch -P0003 -p1
%endif

%ifarch loongarch64
cp %{SOURCE1} .
cp %{SOURCE2} .
cp %{SOURCE3} .
sh ./apply-patches
%endif

sed -i -e '/install -m/s/-m/-p -m/' Makefile

%build
%make_build amalg Q= E=@: PREFIX=%{_prefix} TARGET_STRIP=: \
           CFLAGS="%{optflags}" LDFLAGS="%{__global_ldflags}" \
           MULTILIB=%{_lib}

%install
%make_install PREFIX=%{_prefix} \
              MULTILIB=%{_lib}

rm -rf _tmp_html ; mkdir _tmp_html
cp -a doc _tmp_html/html

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc README COPYRIGHT
%{_bindir}/%{name}
%{_bindir}/%{name}-2.1.ROLLING
%{_libdir}/lib%{name}-*.so.*
%{_datadir}/%{name}-2.1/

%files devel
%doc _tmp_html/html/
%{_includedir}/%{name}-%{apiver}/
%{_libdir}/lib%{name}-*.so
%{_libdir}/pkgconfig/%{name}.pc
%exclude %{_libdir}/*.a

%files help
%{_mandir}/man1/%{name}.1*

%changelog
* Tue Feb 18 2025 zhaoxiaolin <zhaoxiaolin@loongson.cn> - 2.1.0-10
- optimize code and fix some bugs on loongarch64

* Tue Apr 23 2024 zhaoxiaolin <zhaoxiaolin@loongson.cn> - 2.1.0-9
- Add loongarch64 base support

* Thu Mar 21 2024 TexasOct <TexasOct@isrc.iscas.ac.cn> - 2.1.0-8
- update riscv64 support and update to mainline d06beb04

* Wed Jul 19 2023 Jingwiw <wangjingwei@iscas.ac.cn> - 2.1.0-7
- add riscv64 support

* Mon May 15 2023 xu_ping <707078654@qq.com> - 2.1.0-6
- Update upstream commit

* Fri Mar 24 2023 liyanan <liyanan32@h-partners.com> - 2.1.0-5
- Round upstream commit

* Sat Aug 20 2022 wangkai <wangkai385@h-partners.com> - 2.1.0-4
- add secure compile option -fstack-protector-strong

* Wed Jun 30 2021 liuyumeng <liuyumeng5@huawei.com> - 2.1.0-3
- add BuildRequires: gcc

* Mon Feb 8 2021 zhanghua <zhanghua40@huawei.com> - 2.1.0-2
- fix CVE-2020-24372

* Mon Jan 11 2021 zhangatao <zhangtao221@huawei.com> - 2.1.0-1
- fix CVE-2020-15890

* Sun Mar 15 2020 zhangatao <zhangtao221@huawei.com> - 2.1.0-0.8beta3
- package init
